const dbController = require('../controller/dbController')
const router = require('express').Router()

router.get("/tables", dbController.getTables)
router.get("/tablesDescription", dbController.getTablesDescriptions)

module.exports = router;
